from setuptools import setup, Extension
from Cython.Build import cythonize

setup(
    ext_modules = cythonize(Extension(
        "nickel",
        sources=["nickel.pyx"],
        language="c++17",
        extra_compile_args=["-std=c++17"]
)))
