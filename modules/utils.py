## The Utils class provides several utilities, such as print functions, to all other modules.
class Utils:
    ## The constructor initializes members and inherits the config
    def __init__(self, conf, verbose=False):
        ## modules.config.Config object
        self.conf = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose

    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)

    ## Non-verbose printing method (e.g. if output is directed into a file)
    def logprint(self, *printStuff, **etc):
        print(*printStuff, **etc)
