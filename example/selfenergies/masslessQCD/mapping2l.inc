* This file contains the mapping of input diagrams on other diagram topologies


*--#[ d2l1 :

   #define INT1 "d2l1"
   #define MOMREPLACEMENT "multiply replace_(p1,p1, p2,p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l1 :


*--#[ d2l2 :

   #define INT1 "d2l1"
   #define MOMREPLACEMENT "multiply replace_(p1,p1, p2,p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l2 :


*--#[ d2l3 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,p1, p2,p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l3 :


*--#[ d2l4 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,-p5);"

*--#] d2l4 :


*--#[ d2l5 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l5 :


*--#[ d2l6 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l6 :


*--#[ d2l7 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l7 :


*--#[ d2l8 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l8 :


*--#[ d2l9 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l9 :


*--#[ d2l10 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,-p1, p2,-p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l10 :

