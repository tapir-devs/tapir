# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.1.1 for Linux x86 (64-bit) (June 19, 2020)
# Date: Thu 11 Apr 2024 11:32:06


from .object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

