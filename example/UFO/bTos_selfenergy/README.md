# Example: Flavour-changing b-s self-energy at one-loop

As an example with Feynman rules extracted by the `UFOReader` module, let us consider the flavour-changing one-loop $`b \to s`$ self-energy diagrams.
They are mediated by an internal up-type quark and a charged gauge or Goldstone boson and arise e.g. in the calculation of rare $`B_s`$ meson decays.
In total there are 9 diagrams, one for each combination of the three different internal up-type quarks and three different bosons: `WLp`, `WTp`, `Gp`.
The massive gauge bosons are split into "transversal" and "longitudinal" [components](../../../doc/user/UFOReader.md#extracting-propagators).

We will generate these diagrams for the Standard Model, using the Standard Model `tapir` [output](../SM/tapir_SM_UFO/) of the [UFO files](../SM/Standard_Model_UFO/).

For our purpose, we will treat the external $`b`$ and $`s`$ quarks as massless, as well as the internal $`u`$ and $`c`$ quarks.


## Diagrams
First, we need to generate our nine diagrams. For this we use `qgraf`.
Since it uses external software, we do not rely on it here, but provide the resulting `qlist_1lbTosSE` file.
The user can of course decide to generate the diagrams themselves (the complete `qgraf.dat` file is prepended to the list of diagrams in the `qlist_1lbTosSE` file).


## Config file
Next, to get going with `tapir`, we need a [config file](../../../doc/user/config.md). Let us have a look into our config file `1lbTosSE.conf`:
```
* tapir.propagator_file ../SM/tapir_SM_UFO/UFO.prop
* tapir.vertex_file ../SM/tapir_SM_UFO/UFO.vrtx

* tapir.gaugeparam W:xiw

* tapir.mass ftq:M1
* tapir.mass WTp:M2
* tapir.mass WLp:M3
* tapir.mass Gp:M3

* tapir.gaugeprop WLp:WTp
* tapir.gaugeprop WLm:WTp
```

In the first two lines, we instruct `tapir` where the definitions of all propagators and vertices are located.
They will be used in the construction of analytic expressions for the diagrams.

Then, with the line
```
* tapir.gaugeparam W:xiw
```
we tell `tapir` that we would like to call the gauge parameter of the `W` boson `xiw`. This will replace the [template `<gauge_parameter_xiX>`](../../../doc/user/rules.md) by `xiw` everywhere, such that we can refer to this gauge parameter by `xiw` in the remainder of the calculation.

After this, some mass definitions follow. Note that the only fermion mentioned here is the top quark `ftq`, to which we assign the mass `M1`. All other quarks are treated as massless.
The "longitudinal" component and Goldstone boson of the $`W^{\pm}`$ boson have a different mass than the "transversal" component: $`M_3^2 = \xi_W M_2^2 = \xi_W M_W^2`$, where $`M_W \approx 80 \, \mathrm{GeV}`$ is the physical $`W`$-boson mass.

In the last step,
```
* tapir.gaugeprop WLp:WTp
* tapir.gaugeprop WLm:WTp
```
we assure that the "longitudinal" components `WLm` and `WLp` of the `W` propagator are connected to the correct "transversal" ones (and not e.g. to the "transversal" `Z`).


## Generating the analytic expressions
To run `tapir`, we change into the main directory and execute
```bash
./tapir -q example/UFO/bTos_selfenergy/qlist_1lbTosSE -c example/UFO/bTos_selfenergy/1lbTosSE.conf -do example/UFO/bTos_selfenergy/1lbTosSE.dia -eo example/UFO/bTos_selfenergy/1lbTosSE.edia
```
which will use the `qlist_1lbTosSE` and `1lbTosSE.conf` files to produce the `.dia` and `.edia` files.
The structure of [`dia`](../../../doc/user/dia.md) and [`edia`](../../../doc/user/diagramAndTopologyFiles.md) files is described on their respective manpages, so we will not go into details here.

However, a few aspects in the `.dia` file are noteworthy:
```
*--#[ d1l1 :

    (+1)*1
    *Dlong(nu6,nu5,p1,xiw,M2)
    *auxSlash(-p2,j7,j8)
    *(ufoGC121*(auxGamma(nu6,i1,iufo7)*auxPL(iufo7,j7)))
    *(ufoGC60*(auxGamma(nu5,j8,iufo2)*auxPL(iufo2,i2)))
    ;

    #define TOPOLOGY "arb"
    #define INT1 "arb"

*--#] d1l1 :

*--#[ fqcd1l1 :

    1
    *d_(j7,j8)
    *(d_(j7,i1))
    *(d_(i2,j8))
    ;

*--#] fqcd1l1 :
...
```
In the first diagram, a longitudinal gauge boson is exchanged, which has mass `M3`.
However, the propagator `Dlong(nu6,nu5,p1,xiw,M2)` only explicitly carries mass `M2`.
If one uses [`exp`](https://arxiv.org/abs/hep-ph/9905298) in the next step, this is not a problem, as `M3` will be put into the "longitudinal" propagator.
If one does not rely on `exp`, one has to make sure at this step that the "longitudinal" (and Goldstone) propagators are treated correctly.

Another aspect that deserves some attention is the use of spinor, colour and Lorentz indices.
There are some internal indices `iufoX`, `jX` that are contracted, and some open indices:
- the open spinor indices `i1` and `i2` in the Lorentz part of the amplitude
- the open spinor colour indices in the QCD part, which will just evaluate to `d_(i1,i2)`, since we are dealing with a quark two-point function

Often, in such a setup, one would like to solve the $`\gamma`$-matrix algebra by taking traces of the fermion lines.
In our case, one has to apply a projector to construct a "closed fermion line", i.e. contract the open spinor indices in the Lorentz part.
Since we know that the amplitude will eventually be proportional to $`\frac{1}{2} q_1^{\mu} \gamma_{\mu} \left( 1 - \gamma_5 \right)`$ (where $`q_1`$ is the momentum of the $`b`$ quark), we can e.g. play a cheap trick and simply project onto the $`q_1^{\mu} \gamma_{\mu}`$ component in a later part of the calculation, before taking the traces.
```
Symbol vectorpart;
multiply 1/4 * vectorpart * auxGamma(q1,i2,i1) * q1.q1^-1;
```
This will close the $`\gamma`$ matrix trace, but it is still written in terms of spinor indices.
The user can then proceed and `id` these expressions into a chain of ordinary `FORM` $`\gamma`$ matrices and eventually take the trace.


## Using the `UFOdecl.inc` and `UFOrepl.inc` files
At several stages during the calculation, the user might want to include the file [`UFOdecl.inc`](../SM/tapir_SM_UFO/UFOdecl.inc) to automatically define all the various coupling constants `ufoGCXXX` that arise in the diagrams.
The file [`UFOrepl.inc`](../SM/tapir_SM_UFO/UFOrepl.inc) contains replacement rules for these variables expressed in terms of parameters of the Standard Model.
In a calculation with rather long intermediate expressions, it is advisable to use this file only at the end of the calculation to arrive at the final result.
