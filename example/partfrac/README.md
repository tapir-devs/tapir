# Standalone partial fractioning
This example shows how do partial fraction families that are given in Mathematica notation.

## 1. Input file
First, we need an input file with linearly dependent propagators, an example is given in `testsomode.m`:

```bash
{{"b1l", { (p1+q1)^2, (p1+q1+q2)^2, (p1+q2)^2, p1^2} ,{ p1 }},
{"b2l", { (p1+q1)^2, (p1+q1-q2)^2, (p1-q2)^2, p1^2} ,{ p1 }}}
```

The first entry of each list element is the name of the family, the second the list of propagators and the third the list of loop momenta.
Alternatively, the input file `testsomode.yaml` can be used.

## 2. Partial fraction without minimization
To partial fraction the families in `testsomode.m` we call `tapir` with:

```bash
$ ../../tapir partfrac -i testsomode.m -f testsomodeout1
```

This generates two FORM files containing the code to partial fraction integrals of the two families, `testsomodeout1/bl1` and `testsomodeout1/bl1`,
as well as a Mathematica and a YAML file with information on the resulting eight linearly independent families.

## 3. Partial fraction without minimization with minimization
To partial fraction the families in `testsomode.m` and minimize the resulting ones we call `tapir` with:

```bash
$ ../../tapir partfrac -i testsomode.m -f testsomodeout2 -pm
```

This generates two FORM files containing the code to partial fraction integrals of the two families, `testsomodeout1/bl2` and `testsomodeout1/bl2`,
as well as a Mathematica and a YAML file with information on the resulting two linearly independent families.
