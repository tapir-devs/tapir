from modules.qgrafReader import QgrafReader
from modules.diagram import Diagram
from modules.config import Conf
from modules.ediaGenerator import EdiaEntry, EdiaGenerator
from modules.externalLineFilter import ExternalLineFilter
import unittest

class testEdiaGenerator(unittest.TestCase):
    def init(self):

        diagram = Diagram()
        diagram.diagramNumber = 42
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [["q1", 4, "ft1",1], ["q2", 3, "fT1",2], ["q3", 1, "ft2",3], ["q4", 2, "fT2",4]]
        diagram.internalMomenta = [["p1", 1, 2, "fT2", "ft2", 9, 10, ""],\
             ["p2", 3, 1, "g", "g", 11, 12, ""], ["p3", 4, 2, "g", "g", 13, 14, ""], ["p4", 5, 3, "g", "g", 15, 16, ""],\
             ["p5", 4, 6, "fT1", "ft1", 17, 18, ""], ["p6", 6, 5, "g", "g", 19, 20, ""], ["p7", 6, 5, "g", "g", 21, 22, ""]]
        diagram.setName()
        return diagram


    def test_EdiaEntryConstructor(self):
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        ediaEntry = EdiaEntry(config, diagram)

        # test fields
        self.assertEqual(ediaEntry.name, "d2l42")
        self.assertEqual(ediaEntry.numPropagators, 7)
        self.assertEqual(ediaEntry.numLoops, 2)
        self.assertEqual(ediaEntry.numIndependentMomenta, 3)
        self.assertEqual(ediaEntry.externalMomFlow[0], ["q1", "", 4, 2])
        self.assertEqual(ediaEntry.internalMomFlow[0], ["p1", "M1", 1, 2])


    def test_EdiaEntry_toTopselEntry(self):
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        ediaEntry = EdiaEntry(config, diagram)

        # test topsel entry
        self.assertEqual(ediaEntry.toTopselEntry(), "{d2l42;7;2;3;1;;(q1:4,2)(q2:3,2)(q3:1,2)(p1:1,2)(p2:3,1)(p3:4,2)(p4:5,3)(p5:4,6)(p6:6,5)(p7:6,5);1000100}\n")


    def test_EdiaEntry_toTopselEntryWithAbsentMassiveLines(self):
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        config.topoAllowAbsentMassiveLines = True
        ediaEntry = EdiaEntry(config, diagram)

        # test topsel entry
        self.assertEqual(ediaEntry.toTopselEntry(), "{d2l42;7;2;3;1;;(q1:4,2)(q2:3,2)(q3:1,2)(p1:1,2)(p2:3,1)(p3:4,2)(p4:5,3)(p5:4,6)(p6:6,5)(p7:6,5);1000100;1000x00;x000100;x000x00}\n")


    def test_EdiaEntry_toEdiaEntry(self):
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        ediaEntry = EdiaEntry(config, diagram)

        # test edia entry
        self.assertEqual(ediaEntry.toEdiaEntry(), "{d2l42;7;2;3;1;M1;(q1:4,2)(q2:3,2)(q3:1,2)(p1,M1:1,2)(p2:3,1)(p3:4,2)(p4:5,3)(p5,M1:4,6)(p6:6,5)(p7:6,5)}\n")


    def test_EdiaGenerator(self):
        config = Conf("test/qqqq.conf",False)
        qgrafReader = QgrafReader(config)
        diagrams = qgrafReader.getDiagrams("test/qlist.2")
        ediaGenerator = EdiaGenerator(config)

        ediaGenerator.generateEntries(diagrams)

        # simply test correct amount of entries
        self.assertEqual(len(ediaGenerator.entryList), 63)


    def test_tadpoleDiagram_LineCutIntegration(self):
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {"q1":"0", "q2":"0", "q3":"0", "q4":"0"}

        diagram = ExternalLineFilter(config).filter([diagram])[0]

        ediaEntry = EdiaEntry(config, diagram)

        # test edia entry
        self.assertEqual(ediaEntry.toEdiaEntry(), "{d2l42;7;2;0;1;M1;(p1,M1:1,2)(p2:3,1)(p3:4,2)(p4:5,3)(p5,M1:4,6)(p6:6,5)(p7:6,5)}\n")


    def test_differentMassCounting(self):
        diagram = self.init()
        config = Conf("test/massgap.conf",False)

        diagram = ExternalLineFilter(config).filter([diagram])[0]

        ediaEntry = EdiaEntry(config, diagram)

        # test edia entry
        self.assertEqual(ediaEntry.toEdiaEntry(), "{d2l42;7;2;3;4;M2;(q1:4,2)(q2:3,2)(q3:1,2)(p1,M2:1,2)(p2:3,1)(p3:4,2)(p4:5,3)(p5,M2:4,6)(p6:6,5)(p7:6,5)}\n")
