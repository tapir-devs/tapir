from modules.qgrafReader import QgrafReader
from modules.diagram import Diagram
from modules.config import Conf
from modules.momentumAssigner import MomentumAssigner
from sympy import Matrix
import sympy as sy
import unittest
import logging


class testMomentumAssigner(unittest.TestCase):

    def init(self):
        config = Conf("test/qqqq.conf",False)

        # simple massive three-point diagram with one loop
        self.diagram = Diagram()
        self.diagram.numLoops = 1
        self.diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        self.diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "fT1", "ft1", 2,3, "M1"], ["p3", 3,1, "fT1", "ft1", 3,1, "M1"]]
        self.diagram.createTopologicalVertexAndLineLists()
        
        self.mA = MomentumAssigner(config, self.diagram)


    def test_createMomentumMatrix(self):
        self.init()

        # with instance
        matrix = self.mA.createMomentumMatrix(topologicalVertexList = self.diagram.topologicalVertexList, \
                                                    momentumNames = ["p1", "p2", "p3", "q1", "q2", "q3"], \
                                                    externalMomenta = {})

        self.assertListEqual(Matrix([[-1,0,1,1,0,0], [1,-1,0,0,1,0], [0,1,-1,0,0,1]]).rref()[0].tolist(), Matrix(matrix).rref()[0].tolist())

        # static
        matrix = MomentumAssigner.createMomentumMatrix(topologicalVertexList = self.diagram.topologicalVertexList, \
                                                    momentumNames = ["p1", "p2", "p3", "q1", "q2", "q3"], \
                                                    externalMomenta = {})

        self.assertListEqual(Matrix([[-1,0,1,1,0,0], [1,-1,0,0,1,0], [0,1,-1,0,0,1]]).rref()[0].tolist(), Matrix(matrix).rref()[0].tolist())


    def test_extractMomentumDictFromEchelonMatrix(self):
        self.init()
        momentumMatrix = Matrix([[1, 0, 0, 0, 1, 1],[0, 1, 1, -1, 0, 1]])
        momentumNames = ["p1", "p2", "p3", "q1", "q2", "q3"]
        momDict = self.mA.extractMomentumDictFromEchelonMatrix(momentumMatrix, momentumNames)
        self.assertDictEqual(momDict, {"p1": "-q2 - q3", "p2": "-p3 + q1 - q3"})


    def test_findLoopMomenta(self):
        self.init()
        momentumNames = ["p1", "p2", "p3", "q1", "q2", "q3"]
        momentumMatrix = Matrix([[1, 0, 0, 0, 1, 1],[0, 0, 1, -1, 0, 1]])
        externalMomentumNames = ["q1", "q2", "q3"]
        loopMomenta = self.mA.findLoopMomenta(momentumMatrix, momentumNames, externalMomentumNames)
        self.assertListEqual(loopMomenta, ["p2"])


    def test_findRelevantMomentumProducts(self):
        self.init()
        loopMomenta = ["p1","pp6"]
        externalMomenta = ["q12", "Q"]
        pMix, pLoop, pExternal = self.mA.findRelevantMomentumProducts(loopMomenta, externalMomenta)

        pMixTest = [["p1","pp6"], ["p1","Q"], ["p1","q12"], ["pp6","Q"], ["pp6","q12"]]
        pLoopTest = [["p1","p1"], ["pp6","pp6"]]
        pExternalTest = [["Q","Q"], ["Q","q12"], ["q12","q12"]]

        pMix.sort()
        pLoop.sort()
        pExternal.sort()
        pMixTest.sort()
        pLoopTest.sort()
        pExternalTest.sort()

        self.assertListEqual(pMix, pMixTest)
        self.assertListEqual(pLoop, pLoopTest)
        self.assertListEqual(pExternal, pExternalTest)


    def test_squaredMomentumMatrixAndBasisVector1Loop(self):
        self.init()
        A = [[1, 1, 0, 2, 1],[0, 0, 1, -2, 1]]
        p = ["p1", "p2", "p3", "q2", "q3"]
        pMix, pLoop, pExternal = self.mA.findRelevantMomentumProducts(["p2"], ["q2", "q3"])

        lineMasses = {"p1":"M1", "p2":"M1", "p3":"M1"}

        B, P = self.mA.squaredMomentumMatrixAndBasisVector(A, p, pMix, pLoop, pExternal, lineMasses)

        self.assertListEqual(P, [["p2", "q3"], ["p2", "q2"], ["p1", "p1"], ["p3", "p3"], ["p2", "p2"], ["q2", "q2"], ["q2", "q3"], ["q3", "q3"]])
        self.assertListEqual(B.tolist(), [[2,4,-1,0,1,4,4,1],[0,0,0,-1,0,4,-4,1]])
        

    def test_squaredMomentumMatrixAndBasisVector2Loop(self):
        self.init()
        A = [[1, 0, 0, 0, 0, 0, 1, 0, -1, -1, -1], [0, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0], [0, 0, 1, 0, 0, -1, -1, 0, 0, 1, 0], [0, 0, 0, 1, 0, -1, -1, 0, 0, 1, 1], [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0]]
        p = ['p6', 'p4', 'p1', 'p2', 'p3', 'p5', 'p7', 'q4', 'q3', 'q2', 'q1']
        pMix = [['p7', 'q1'], ['p7', 'q2'], ['p7', 'q3'], ['p5', 'q1'], ['p5', 'q2'], ['p5', 'q3'], ['p5', 'p7']]
        pLoop = [['p5', 'p5'], ['p7', 'p7']]
        pExternal = [['q3', 'q3'], ['q3', 'q2'], ['q3', 'q1'], ['q2', 'q2'], ['q2', 'q1'], ['q1', 'q1']]
        lineMasses = {'p2':0, 'p3':0, 'p5':0, 'p7':0, 'p1':'M1', 'p4':'M1', 'p6':'M1'}

        Ac = A.copy()
        pc= p.copy()
        pMixc = pMix.copy()
        pLoopc = pLoop.copy()
        pExternalc = pExternal.copy()
        lineMassesc = lineMasses.copy()

        B, P = self.mA.squaredMomentumMatrixAndBasisVector(Ac, pc, pMixc, pLoopc, pExternalc, lineMasses)

        self.assertEqual(A, Ac)
        self.assertEqual(p, pc)
        self.assertEqual(pMix, pMixc)
        self.assertEqual(pLoop, pLoopc)
        self.assertEqual(pExternal, pExternalc)
        self.assertEqual(lineMasses, lineMassesc)

        testDict = {"q1" : 1, "q2" : 5, "q3" : 9, "p7" : 17, "p5" : 23}
        testDict["p6"] = - testDict["p7"] + testDict["q1"] + testDict["q2"] + testDict["q3"]
        testDict["p4"] = testDict["p5"] + testDict["q3"]
        testDict["p1"] = testDict["p5"] + testDict["p7"] - testDict["q2"]
        testDict["p2"] = testDict["p5"] + testDict["p7"] - testDict["q1"] - testDict["q2"]
        testDict["p3"] = -testDict["p5"] - testDict["p7"]

        A = sy.Matrix(A) * sy.Matrix(sy.symbols(" ".join(p)))
        PP = [Pentry[0] + "*" + Pentry[1] for Pentry in P]
        B = B * sy.Matrix(sy.symbols(" ".join(PP)))

        for row in A.tolist():
            eq = str(row[0])
            for key, val in testDict.items():
                eq = eq.replace(key, str(val))
            self.assertEqual( eval(eq), 0)

        for row in B.tolist():
            eq = str(row[0])
            for key, val in testDict.items():
                eq = eq.replace(key, "("+str(val) + ")")
            self.assertEqual( eval(eq), 0)
        



    def test_getMomentumProductsDict(self):
        self.init()
        B = Matrix([[2,4,-1,0,1,4,4,1],[0,0,0,-1,0,4,-4,1]])
        P = [["p2", "q3"], ["p2", "q2"], ["p1", "p1"], ["p3", "p3"], ["p2", "p2"], ["q2", "q2"], ["q2", "q3"], ["q3", "q3"]]
        externalMomenta = ["q2", "q3"]

        productDict = self.mA.getMomentumProductsDict(B, P, externalMomenta)

        expr = "-2*p2.q2 + p1.p1/2 - p2.p2/2 - 2*q2.q2 - 2*q2.q3 - q3.q3/2"
        expr1 = sy.sympify(productDict["p2.q3"].replace(".", "*"))
        expr2 = sy.sympify(expr.replace(".", "*"))
        self.assertEqual(expr1, expr2)

        expr = "4*q2.q2 - 4*q2.q3 + q3.q3"
        expr1 = sy.sympify(productDict["p3.p3"].replace(".", "*"))
        expr2 = sy.sympify(expr.replace(".", "*"))
        self.assertEqual(expr1, expr2)


    def test_findEqualMomentumSquaredProducts (self):
        self.init()

        lineMasses = {"p" + str(i) : 0 for i in range(20)}
        lineMasses["p6"] = "M1"
        lineMasses["p8"] = "M1"
        lineMasses["p9"] = "M1"
        lineMasses["p10"] = "M1"
        lineMasses["p11"] = "M10"
        lineMasses["p12"] = "M10"

        subs = self.mA.findEqualMomentumSquaredProducts(
            {"p1.p1": "p2.p2-q1.q1", "p2.p2": "p2.p2-q1.q1", "p3.p3": "p2.p2", "p4.p4": "-p2.p2+q1.q1", "p4.p5": "q2.q2", "p6.p6": "p2.p2-q1.q1",
             "p7.p7": "p2.p2-q1.q1", "p8.p8" : "p2.p2-q1.q1", "p9.p9": "p2.p2", "p10.p10": "p6.p6", "p11.p11": "p2.p2-q1.q1", "p12.p12": "p2.p2-q1.q1",
             "p13.p13":"q2.q2", "q1.q1":"q2.q2"},
            lineMasses
            )

        self.assertDictEqual(subs, {'p10.p10': 'p6.p6', 'p3.p3': 'p2.p2', 'p7.p7': 'p1.p1', 'p8.p8': 'p6.p6', "p12.p12": "p11.p11", "q1.q1":"q2.q2", "p13.p13":"q2.q2"})

        # massless on massive - direct
        self.assertDictEqual(self.mA.findEqualMomentumSquaredProducts({"p1.p1":"p2.p2", "p3.p3":"p1.p1"}, {"p1":0, "p2":"M1", "p3":"M2"}), {"p1.p1":"p2.p2"})

        # massless on massive - indirect
        self.assertDictEqual(self.mA.findEqualMomentumSquaredProducts({"p1.p1":"p2.p2-q1.q1", "p3.p3":"p2.p2-q1.q1", "p4.p4":"p2.p2-q1.q1"}, {"p1":0, "p2":"M1", "p3":"M1", "p4":"M2"}), {"p1.p1":"p3.p3"})



    def test_generateLineMassesDict(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "g", "g", 2,3, ""], ["p3", 3,1, "fT1", "ft1", 3,1, "M1"], ["p4", 1,3, "w", "W", 1,3, "M2"]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"ft1": "M1", "W": "M2"}
        
        mA = MomentumAssigner(conf, diagram)

        mA.generateLineMassesDict()

        self.assertDictEqual(mA.lineMasses, {"p1":"M1", "p2":0, "p3":"M1", "p4":"M2"})


    def test_calculateMomentumRelations(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "g", "g", 2,3, ""], ["p3", 3,1, "fT1", "ft1", 3,1, "M1"]]
        diagram.createTopologicalVertexAndLineLists()
        
        conf = Conf("test/qqqq.conf",False)
        conf.externalMomenta = {}

        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()

        self.assertDictEqual(mA.momentumDict, {'p1': 'p3 + q1', 'p2': 'p3 + q1 + q2'}, "Different definitions are possible!")

        self.assertListEqual(mA.loopMomenta, ["p3"])

        self.assertListEqual(sorted(mA.irreducibleExternalMomenta), sorted(["q1", "q2"]))
        self.assertListEqual(sorted(mA.externalMomenta), sorted(["q1", "q2", "q3"]))

        self.assertDictEqual(mA.momentumProducts, {'p3.q1': 'p1.p1/2 - p3.p3/2 - q1.q1/2', 'p3.q2': '-p1.p1/2 + p2.p2/2 - q1.q2 - q2.q2/2'}, "May differ")
        self.assertDictEqual(mA.equalMomentumSquaredProducts, {})
        self.assertListEqual(mA.topoFactors, ['s3m1', 's1m1', '1/p2.p2'])
        self.assertListEqual(mA.topoFactorSigns, ['', '', '-'])
        self.assertListEqual([sy.sympify(expr).expand() for expr in mA.analyticTopologyExpression], 
        [sy.sympify('M1^2 - p3^2').expand(),
        sy.sympify('M1^2 - p3^2 - 2*p3*q1 - q1^2').expand(),
        sy.sympify('-p3^2 - 2*p3*q1 - 2*p3*q2 - q1^2 - 2*q1*q2 - q2^2').expand()])


    def test_computeTopoFactorsWithEqualMomenta(self):
        diagram = Diagram()
        diagram.externalMomenta = [
            ['q1', 1, 'g', 1],
            ['q2', 3, 'g', 3]
        ]

        diagram.internalMomenta = [
            ['p1', 4, 2, 'g', 'g', 4, 2, ''],
            ['p2', 5, 3, 'g', 'g', 6, 3, ''],
            ['p3', 3, 6, 'ft1', 'fT1', 3, 7, 'M1'],
            ['p4', 4, 1, 'ft1', 'fT1', 4, 1, 'M1'],
            ['p5', 1, 5, 'ft1', 'fT1', 1, 6, 'M1'],
            ['p6', 5, 4, 'ft1', 'fT1', 6, 4, 'M1'],
            ['p7', 6, 2, 'g', 'g', 7, 5, ''],
            ['p8', 6, 2, 'g', 'g', 7, 5, '']
        ]

        diagram.createTopologicalVertexAndLineLists()
        
        conf = Conf("test/qqqq.conf",False)
        conf.externalMomenta = {}
        conf.mass = {"ft1": "M1"}

        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()

        self.assertDictEqual(mA.equalMomentumSquaredProducts, {"p1.p1": "p3.p3"})
        self.assertIn('s6m1', mA.topoFactors)
        self.assertIn('s5m1', mA.topoFactors)
        self.assertIn('s4m1', mA.topoFactors)
        self.assertIn('s3m1', mA.topoFactors)
        self.assertFalse('1/p1.p1' in mA.topoFactors)
        self.assertIn('1/p3.p3', mA.topoFactors)


    def test_getEikonalProducts(self):
        self.assertListEqual([["p1","q1"], ["p1","q2"], ["p4","q1"], ["p4","q2"], ["p5","q1"], ["p5","q2"]], MomentumAssigner.getEikonalProducts(["p1","p4","p5"], ["q1","q2"], externalEikonal=True))

        self.assertListEqual([["p1","p2"], ["p1","p4"], ["p2","p4"]], MomentumAssigner.getEikonalProducts(["p1", "p2", "p4"], ["q1","q2"], externalEikonal=False))

    
    def test_findEqualMomenta(self):
        momentumDict = {"p2":"p1-q1", "p3":"-q1+p1", "p4":"-p1+q1", "p5":"-q1", "p7":"p6", "p8":"p1-q1-q2", "p9":"p1+q1", "p10":"-p1-q1"}
        lineMasses = {"p1":0, "p2":0, "p3":"M1", "p4":0, "p5":0, "p6":0, "p7":0, "p8":0, "p9":0, "p10":0}
        self.assertDictEqual(MomentumAssigner.findEqualMomenta(momentumDict, lineMasses), {"p2":"p3", "p4":"-p3", "p5":"-q1", "p7":"p6", "p10":"-p9"})


    def test_analyticTopologyExpressionWithEikonals(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 4, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, "M1"], ["p2", 1,3, "f2", "f2", 1,3, "M2"], ["p3", 2,3, "f3", "f3", 2,3, "M3"], ["p4", 2,5, "g", "g", 2,5, ""], ["p5", 3,4, "g", "g", 3,4, ""], ["p6", 5,4, "f1", "f1", 5,4, "M1"]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"f1": "M1", "f2": "M2", "f3": "M3"}

        conf.topoEikonalExternal = [False]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p5.p5", "1/p6.p6"]))

        conf.topoEikonalExternal = [True]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.q1", "1/p2.q1", "1/p3.q1", "1/p5.q1", "1/p6.q1"]))

        conf.topoEikonalExternal = [True, "v1"]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.v1", "1/p2.v1", "1/p3.v1", "1/p5.v1", "1/p6.v1"]))

        conf.topoEikonalExternal = [False]
        conf.topoEikonalInternal = True
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.p2", "1/p1.p3", "1/p1.p5", "1/p1.p6", "1/p2.p3", "1/p2.p5", "1/p2.p6", "1/p3.p5", "1/p3.p6", "1/p5.p6"]))

        conf.topoEikonalExternal = [True]
        conf.topoEikonalInternal = True
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.p2", "1/p1.p3", "1/p1.p5", "1/p1.p6", "1/p2.p3", "1/p2.p5", "1/p2.p6", "1/p3.p5", "1/p3.p6", "1/p5.p6", "1/p1.q1", "1/p2.q1", "1/p3.q1", "1/p5.q1", "1/p6.q1"]))


    def test_analyticTopologyExpressionWithSelectedExternalEikonals(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 1, "fT1", 1], ["q3", 4, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, "M1"], ["p2", 1,3, "f2", "f2", 1,3, "M2"], ["p3", 2,3, "f3", "f3", 2,3, "M3"], ["p4", 2,5, "g", "g", 2,5, ""], ["p5", 3,4, "g", "g", 3,4, ""], ["p6", 5,4, "f1", "f1", 5,4, "M1"]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"f1": "M1", "f2": "M2", "f3": "M3"}

        conf.topoEikonalExternal = [True, "q2"]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.q2", "1/p2.q2", "1/p3.q2", "1/p5.q2", "1/p6.q2"]))

        conf.topoEikonalExternal = [True, "q1"]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.q1", "1/p2.q1", "1/p3.q1", "1/p5.q1", "1/p6.q1", "1/p3.q2", "1/p6.q2"])) # the last to entries are numerators

        conf.topoEikonalExternal = [True, "q1", "q2"]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.q1", "1/p2.q1", "1/p3.q1", "1/p5.q1", "1/p6.q1", "1/p1.q2", "1/p2.q2", "1/p3.q2", "1/p5.q2", "1/p6.q2"]))

        conf.topoEikonalExternal = [True]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(["s1m1", "s2m2", "s3m3", "s6m1", "1/p6.p6", "1/p5.p5", "1/p1.q1", "1/p2.q1", "1/p3.q1", "1/p5.q1", "1/p6.q1", "1/p1.q2", "1/p2.q2", "1/p3.q2", "1/p5.q2", "1/p6.q2"]))


    def test_analyticTopologyExpressionMixedWithEikonals(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 4, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, ""], ["p2", 1,3, "f2", "f2", 1,3, ""], ["p3", 2,3, "f3", "f3", 2,3, ""], ["p4", 2,4, "g", "g", 2,4, ""], ["p5", 3,4, "g", "g", 3,4, ""], ["p6", 2,4, "f1", "f1", 2,4, ""]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)

        conf.topoEikonalExternal = [False]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertListEqual(sorted(mA.topoFactors), sorted(['1/p1.p1', '1/p3.p3', '1/p4.p4', '1/p2.p2', '1/p5.p5', '1/p6.p6', '1/p1.p4', '1/p1.p3', '1/p4.q1']))

        conf.topoEikonalExternal = [True]
        conf.topoEikonalInternal = False
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertIn('1/p1.p4', mA.topoFactors)
        self.assertIn('1/p1.p3', mA.topoFactors)
        self.assertEqual([p for p in mA.topoFactors if mA.topoFactors.count(p)==1], mA.topoFactors)

        conf.topoEikonalExternal = [False]
        conf.topoEikonalInternal = True
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertIn('1/p4.q1', mA.topoFactors)
        self.assertEqual([p for p in mA.topoFactors if mA.topoFactors.count(p)==1], mA.topoFactors)


        conf.topoEikonalExternal = [True]
        conf.topoEikonalInternal = True
        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        self.assertIn('1/p1.p4', mA.topoFactors)
        self.assertEqual([p for p in mA.topoFactors if mA.topoFactors.count(p)==1], mA.topoFactors)

    
    def test_kinematicRelation(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, "M1"], ["p2", 1,2, "f1", "f1", 1,2, "M1"]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"f1": "M1"}
        conf.kinematicRelations = {"q1^2": "M1^2"}

        mA = MomentumAssigner(conf, diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()

        self.assertIn('M1^2 - p2^2', mA.analyticTopologyExpression)
        self.assertIn('-p2^2 + 2*p2*q1', mA.analyticTopologyExpression)