#!/usr/bin/env bash

# Quit on error
set -e
# Print executed commands
set -v

# Compare q2e and tapir output
./tapir -q2e -q test/qlist.q2eCompare -c test/q2eCompare.conf -do test/q2eCompare/tapir.dia -eo test/q2eCompare/tapir.edia
diff test/q2eCompare/tapir.edia test/q2eCompare/q2e.edia
diff test/q2eCompare/tapir.dia test/q2eCompare/q2e.dia

# Clear generated test directory
rm -rf test/q2eCompare/tapir.dia test/q2eCompare/tapir.edia

echo -e "\n\e[1;32m[PASS]\e[0;32m Legacy consistency tests succeeded!\e[m\n"
