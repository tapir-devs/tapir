from modules.diagram import Diagram
from modules.config import Conf
from modules.ediaGenerator import EdiaGenerator
from modules.diaGenerator import DiaGenerator
import unittest

class testConfig(unittest.TestCase):
    # Initialize a fake command line arguments (parse_args) object
    def prepareArgs(self):
        class Args:
            def __init__(self):
                self.kernels = 1
                self.qlist = None
                self.diaout = None
                self.ediaout = None
                self.topselin = None
                self.topselout = None
                self.ordertopsel = False
                self.topologyfolder = None
                self.minimize = None
                self.minimize_fine = None
                self.partfrac = False
                self.partfrac_minimize = None
                self.diagramart = None
                self.topologyart = None
                self.repart = None
                self.qlist_out = None
                self.diagram_yaml_out = None
                self.topology_yaml_out = None
                self.diagram_yaml_in = None
                self.topology_yaml_in = None
                self.commands = None
        return Args()


    def test_readConfigFile(self, file="test/conf-test.conf"):
        args = self.prepareArgs()
      
        conf = Conf(file, argsObj=args, oldq2e=False, onlySkeleton=False)

        self.assertEqual(args.kernels, 5)
        self.assertEqual(args.qlist, "qlist.")
        self.assertEqual(args.diaout,  "test.dia")
        self.assertEqual(args.ediaout,  "test.edia")
        self.assertEqual(args.topselin,  "in.topsel")
        self.assertEqual(args.topselout,  "out.topsel")
        self.assertEqual(args.ordertopsel, True)
        self.assertEqual(args.topologyfolder,  "topofolder/topos")
        self.assertEqual(args.minimize,  "topselm1")
        self.assertEqual(args.minimize_fine,  "topselm2")
        self.assertEqual(args.partfrac, True)
        self.assertEqual(args.partfrac_minimize,  "firelist.m")
        self.assertEqual(args.diagramart,  "artsy1.tex")
        self.assertEqual(args.topologyart,  "artsy2.tex")
        self.assertEqual(args.repart,  "artsy3.tex")
        self.assertEqual(args.qlist_out, "qlistOut")
        self.assertEqual(args.diagram_yaml_out, "diaOut.yaml")
        self.assertEqual(args.topology_yaml_out, "topoOut.yaml")
        self.assertEqual(args.diagram_yaml_in, "diaIn.yaml")
        self.assertEqual(args.topology_yaml_in, "topoIn.yaml")

        self.assertEqual(conf.spinorIndices, False)
        self.assertEqual(conf.propFile, "test/qcd.prop")
        self.assertEqual(conf.vrtxFile, "test/qcd.vrtx")
        self.assertListEqual(conf.scales, ["M1","M2","M3"])
        self.assertDictEqual(conf.mass, {"fb": "M1", "fs": "M2", "fc": "M3"})
        self.assertListEqual(conf.expandNaive, ["M1", "M2"])
        self.assertDictEqual(conf.closedFermionLoop, {"fb" : "3*nl"})
        self.assertEqual(conf.blockSize, 42)
        self.assertDictEqual(conf.antiFermion, {"fs" : "fS", "fb" : "fB"})
        self.assertDictEqual(conf.externalMomenta, {"q2":"0", "q3":"0"})
        self.assertDictEqual(conf.drawingObjects, {"g":"gluon", "c":"ghost", "C":"ghost",\
         "sigma":"scalar", "fs":"fermion", "fS":"fermion", "fb":"fermion", "fB":"fermion",\
          "wQSt":"scalar", "wQStF":"scalar"})
        self.assertDictEqual(conf.drawingNames, {"c":r"$c_{gh}$", "C":r"$c_{gh}$", \
         "sigma":r"$\sigma$", "fs":r"$s$", "fS":r"$\bar{s}$", "fb":r"$b$", "fB":r"$\bar{b}$", \
          "wQSt":r"$\sigma_{\tilde Q_S}$", "wQStF":r"$\sigma_{\tilde Q_S^F}$"})
        self.assertEqual(conf.topologyName, "INT1äöß?exit(0)")
        self.assertEqual(conf.topoRemoveDuplicateLines, True)
        self.assertEqual(conf.extendAnalyticalTopos, True)
        self.assertEqual(conf.topoEikonalExternal, [True, "q1", "v1"])
        self.assertEqual(conf.topoEikonalInternal, True)
        self.assertEqual(conf.sigmaParticles, ["sig1","sig2"])
        self.assertDictEqual(conf.kinematicRelations, {"q1.q1": "-2*s/3", "q1.q2": "t"})
        self.assertIn([ "cuts",[ "True", "q1,q2", "h1,h2", "1,1"], ["False" , "q1,q2"], ["1" , "q1,q2" , "ft" , "0,0,1,3,5,7,11,12"] ], conf.filters)
        self.assertIn(['self_energy_bridge_mixing', 'True'], conf.filters)
        self.assertIn(['self_energy_bridge', 'True'], conf.filters)
        self.assertIn(['external_self_energy_bridge_mixing', 'True'], conf.filters)
        self.assertIn(['external_self_energy_bridge', 'True'  ], conf.filters)
        self.assertEqual(conf.mappingFile, "myMapping.inc")
        self.assertEqual(conf.yamlMappingFile, "myYamlMapping.yaml")
        self.assertEqual(conf.topoSortFunc, "ACCU()")
        self.assertEqual(conf.topoCompleteMomentumProducts, False) # Must be false due to topoEikonalExternal and topoEikonalInternal being specified
        self.assertEqual(conf.topoIgnoreBridges, True)
        self.assertEqual(conf.euclideanMomenta, True)


    def test_readYAMLConfigFile(self):
        self.test_readConfigFile(file="test/conf-test.yaml")
