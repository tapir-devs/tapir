from modules.diagram import Diagram
from modules.config import Conf
from modules.ediaGenerator import EdiaGenerator
from modules.diaGenerator import DiaGenerator
from modules.feynmanRulesReader import FeynmanRulesReader
from modules.feynmanRules import FeynmanRules
import unittest

class testDiaGenerator(unittest.TestCase):
    def test_1lgluonprop(self):
        diagram = Diagram()
        diagram.diagramNumber = 777
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "g", 2]]
        diagram.internalMomenta = [["p1", 2, 1, "fT", "ft", 5, 6, "M1"], ["p2", 1, 2, "fT", "ft", 7, 8, "M1"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()
        
	# onlySkeleton needs to be set to avoid computing the flavour dictionary without spinorIndices set to false
        config = Conf("test/topprop3l.conf",onlySkeleton=True)
        config.spinorIndices = False
        config.read("test/topprop3l.conf")

        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])
        resultFolds = []
        resultFolds.append("*--#[ d1l777 :")
        resultFolds.append("")
        resultFolds.append("	(-1)*1*nh")
        resultFolds.append("	*FT1(mu2)")
        resultFolds.append("	*FT1(p2)")
        resultFolds.append("	*FT1(mu1)")
        resultFolds.append("	*FT1(p1)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d1l777 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd1l777 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	*GM(a(2),j5,j8)")
        resultFolds.append("	*d_(j8,j7)")
        resultFolds.append("	*GM(a(1),j7,j6)")
        resultFolds.append("	*d_(j6,j5)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd1l777 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed1l777 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed1l777 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few1l777 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few1l777 :")
        resultFolds.append("")
        resultFolds.append("")

        # 1 Fermion loop
        self.assertListEqual(config.scales, ['M1'])
        self.assertEqual(len(diagram.fermionLines),1)
        self.assertEqual(diagram.numLoops,1)
        self.assertListEqual([l.loop for l in diagram.fermionLines], [True])    
        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))   
        
    def test_2lquarkprop(self):
        diagram = Diagram()
        diagram.diagramNumber = 7
        diagram.preFactor = "(+1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [["q1", 1, "fq", 1], ["q2", 2, "fQ", 2]]
        diagram.internalMomenta = [["p1", 2, 1, "g", "g", 5, 6, ""], ["p2", 1, 3, "fQ", "fq", 7, 8, "M1"], ["p3", 4, 2, "fQ", "fq",9, 10, "M1"], ["p4", 3, 4, "fQ", "fq", 11, 12, "M1"],["p5", 4, 3, "g", "g", 13, 14, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        config = Conf("test/topprop3l.conf",True, oldq2e=True)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])
        resultFolds = []
        resultFolds.append("*--#[ d2l7 :")
        resultFolds.append("")
        resultFolds.append("	(+1)*1")
        resultFolds.append("	*FT1(nu5)")
        resultFolds.append("	*FT1(p3)")
        resultFolds.append("	*FT1(nu13)")
        resultFolds.append("	*FT1(p4)")
        resultFolds.append("	*FT1(nu14)")
        resultFolds.append("	*FT1(p2)")
        resultFolds.append("	*FT1(nu6)")
        resultFolds.append("	*Dg(nu5,nu6,p1)")
        resultFolds.append("	*Dg(nu13,nu14,p5)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d2l7 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd2l7 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	*GM(b(5),i2,j10)")
        resultFolds.append("	*d_(j10,j9)")
        resultFolds.append("	*GM(b(13),j9,j12)")
        resultFolds.append("	*d_(j12,j11)")
        resultFolds.append("	*GM(b(14),j11,j8)")
        resultFolds.append("	*d_(j8,j7)")
        resultFolds.append("	*GM(b(6),j7,i1)")
        resultFolds.append("	*prop(b(5),b(6))")
        resultFolds.append("	*prop(b(13),b(14))")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd2l7 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed2l7 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed2l7 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few2l7 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few2l7 :")
        resultFolds.append("")
        resultFolds.append("")

        # 1 Fermion line
        self.assertListEqual(config.scales, ['M1'])
        self.assertEqual(len(diagram.fermionLines),1)
        self.assertListEqual([l.loop for l in diagram.fermionLines], [False])
        self.assertListEqual([l.isFlipped for l in diagram.fermionLines], [True])
        self.assertListEqual([l.flavours for l in diagram.fermionLines], [[]])
        self.assertListEqual([[k.momentum for k in l.lines] for l in diagram.fermionLines],[['p3','p4','p2']])
        self.assertListEqual([[[k.start, k.end] for k in l.lines] for l in diagram.fermionLines],[[[4, 2],[3, 4],[1, 3]]])
        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))

    def test_2lquarkprop_newfermions(self):
        diagram = Diagram()
        diagram.diagramNumber = 7
        diagram.preFactor = "(+1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [["q1", 1, "fq", 1], ["q2", 2, "fQ", 2]]
        diagram.internalMomenta = [["p1", 2, 1, "g", "g", 5, 6, ''], ["p2", 1, 3, "fQ", "fq", 7, 8, ''], ["p3", 4, 2, "fQ", "fq",9, 10, ''],\
                                   ["p4", 3, 4, "fQ", "fq", 11, 12, ''],["p5", 4, 3, "g", "g", 13, 14, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        config = Conf("test/newfermions.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFolds = []
        resultFolds.append("*--#[ d2l7 :")
        resultFolds.append("")
        resultFolds.append("	(+1)*1")
        resultFolds.append("	*Dg(nu5,nu6,p1)")
        resultFolds.append("	*fprop(p2,nl,nl,j8,j7)")
        resultFolds.append("	*fprop(p3,nl,nl,j10,j9)")
        resultFolds.append("	*fprop(p4,nl,nl,j12,j11)")
        resultFolds.append("	*Dg(nu13,nu14,p5)")
        resultFolds.append("	*fvertV(nl,nl,nu6,j7,i1)")
        resultFolds.append("	*fvertV(nl,nl,nu5,i2,j10)")
        resultFolds.append("	*fvertV(nl,nl,nu14,j11,j8)")
        resultFolds.append("	*fvertV(nl,nl,nu13,j9,j12)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d2l7 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd2l7 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	*prop(b(5),b(6))")
        resultFolds.append("	*d_(j8,j7)")
        resultFolds.append("	*d_(j10,j9)")
        resultFolds.append("	*d_(j12,j11)")
        resultFolds.append("	*prop(b(13),b(14))")
        resultFolds.append("	*GM(b(6),j7,i1)")
        resultFolds.append("	*GM(b(5),i2,j10)")
        resultFolds.append("	*GM(b(14),j11,j8)")
        resultFolds.append("	*GM(b(13),j9,j12)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd2l7 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed2l7 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed2l7 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few2l7 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few2l7 :")
        resultFolds.append("")
        resultFolds.append("")

        self.assertEqual(config.spinorIndices, True)
        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))

    def test_3lquarkprop(self):
        diagram = Diagram()
        diagram.diagramNumber = 44
        diagram.preFactor = "(+1)*1"
        diagram.numLoops = 3
        diagram.externalMomenta = [["q1", 2, "fq", 1], ["q2", 1, "fQ", 2]]
        diagram.internalMomenta = [["p1", 3, 1, "fQ", "fq", 5, 6, "M1"], ["p2", 4, 1, "g", "g", 7, 8, ""], ["p3", 3, 2, "g", "g",9, 10, ""], ["p4", 2, 5, "fQ", "fq", 11, 12, "M1"],\
                                   ["p5", 4, 3, "fQ", "fq", 13, 14, "M1"], ["p6", 6, 4, "fQ", "fq", 14, 15, "M1"], ["p7", 5, 6, "fQ", "fq", 16, 17, "M1"], ["p8", 6, 5, "g", "g", 18, 19, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        config = Conf("test/topprop3l.conf",True, oldq2e=True)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFolds = []
        resultFolds.append("*--#[ d3l44 :")
        resultFolds.append("")
        resultFolds.append("	(+1)*1")
        resultFolds.append("	*FT1(nu8)")
        resultFolds.append("	*FT1(p1)")
        resultFolds.append("	*FT1(nu9)")
        resultFolds.append("	*FT1(p5)")
        resultFolds.append("	*FT1(nu7)")
        resultFolds.append("	*FT1(p6)")
        resultFolds.append("	*FT1(nu19)")
        resultFolds.append("	*FT1(p7)")
        resultFolds.append("	*FT1(nu20)")
        resultFolds.append("	*FT1(p4)")
        resultFolds.append("	*FT1(nu10)")
        resultFolds.append("	*Dg(nu7,nu8,p2)")
        resultFolds.append("	*Dg(nu9,nu10,p3)")
        resultFolds.append("	*Dg(nu19,nu20,p8)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d3l44 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd3l44 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	*GM(b(8),i2,j6)")
        resultFolds.append("	*d_(j6,j5)")
        resultFolds.append("	*GM(b(9),j5,j14)")
        resultFolds.append("	*d_(j14,j13)")
        resultFolds.append("	*GM(b(7),j13,j16)")
        resultFolds.append("	*d_(j16,j15)")
        resultFolds.append("	*GM(b(19),j15,j18)")
        resultFolds.append("	*d_(j18,j17)")
        resultFolds.append("	*GM(b(20),j17,j12)")
        resultFolds.append("	*d_(j12,j11)")
        resultFolds.append("	*GM(b(10),j11,i1)")
        resultFolds.append("	*prop(b(7),b(8))")
        resultFolds.append("	*prop(b(9),b(10))")
        resultFolds.append("	*prop(b(19),b(20))")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd3l44 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed3l44 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed3l44 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few3l44 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few3l44 :")
        resultFolds.append("")
        resultFolds.append("")

        # 1 Fermion line
        self.assertEqual(len(diagram.fermionLines),1)
        self.assertListEqual([l.loop for l in diagram.fermionLines], [False])
        self.assertListEqual([l.isFlipped for l in diagram.fermionLines], [True])
        self.assertListEqual([l.flavours for l in diagram.fermionLines], [[]])
        self.assertListEqual([[k.momentum for k in l.lines] for l in diagram.fermionLines],[['p1','p5','p6','p7','p4']])
        self.assertListEqual([[[k.start, k.end] for k in l.lines] for l in diagram.fermionLines],[[[3, 1],[4, 3],[6, 4],[5, 6],[2, 5]]])
        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))

    def test_3ltopprop(self):
        diagram = Diagram()
        diagram.diagramNumber = 85
        diagram.preFactor = "(+1)*1"
        diagram.numLoops = 3
        diagram.externalMomenta = [["q1", 1, "ft", 1], ["q2", 2, "fT", 2]]
        diagram.internalMomenta = [["p1", 1, 2, "fT", "ft", 5, 6, "M1"],\
             ["p2", 3, 1, "g", "g", 7, 8, ""], ["p3", 4, 2, "g", "g", 9, 10, ""], ["p4", 5, 3, "fT", "ft", 11, 12, "M1"],\
             ["p5", 3, 5, "fT", "ft", 13, 14, "M1"], ["p6", 6, 4, "fQ", "fq", 15, 16, "M1"], ["p7", 4, 6, "fQ", "fq", 17, 18, "M1"], ["p8", 6, 5, "g", "g", 19, 20, ""]]
        diagram.setName()

        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()
        # 2 Fermion loops, 1 Fermion line
        self.assertEqual(len(diagram.fermionLines),3)
        self.assertListEqual([l.loop for l in diagram.fermionLines], [False, True, True])
        self.assertListEqual([l.isFlipped for l in diagram.fermionLines], [True, True, True])
        # Line has no flavour, first loop ft, second loop fq
        self.assertListEqual([l.flavours for l in diagram.fermionLines], [[], ['fT', 'ft'], ['fQ', 'fq']])
        self.assertListEqual([[k.momentum for k in l.lines] for l in diagram.fermionLines],[['p1'], ['-p4', '-p5'], ['-p6', '-p7']])
        self.assertListEqual([[[k.start, k.end] for k in l.lines] for l in diagram.fermionLines],[[[1, 2]], [[3, 5], [5, 3]], [[4, 6], [6, 4]]])

        # Now test code generation
        config = Conf("test/topprop3l.conf",True, oldq2e=True)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFolds = []
        resultFolds.append("*--#[ d3l85 :")
        resultFolds.append("")
        resultFolds.append("	(+1)*1*nh*nl")
        resultFolds.append("	*FT1(nu10)")
        resultFolds.append("	*FT1(p1)")
        resultFolds.append("	*FT1(nu8)")
        resultFolds.append("	*FT2(nu20)")
        resultFolds.append("	*FT2(p5)")
        resultFolds.append("	*FT2(nu7)")
        resultFolds.append("	*FT2(p4)")
        resultFolds.append("	*FT3(nu19)")
        resultFolds.append("	*FT3(p7)")
        resultFolds.append("	*FT3(nu9)")
        resultFolds.append("	*FT3(p6)")
        resultFolds.append("	*Dg(nu7,nu8,p2)")
        resultFolds.append("	*Dg(nu9,nu10,p3)")
        resultFolds.append("	*Dg(nu19,nu20,p8)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d3l85 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd3l85 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	*GM(b(10),i2,j6)")
        resultFolds.append("	*d_(j6,j5)")
        resultFolds.append("	*GM(b(8),j5,i1)")
        resultFolds.append("	*GM(b(20),j11,j14)")
        resultFolds.append("	*d_(j14,j13)")
        resultFolds.append("	*GM(b(7),j13,j12)")
        resultFolds.append("	*d_(j12,j11)")
        resultFolds.append("	*GM(b(19),j15,j18)")
        resultFolds.append("	*d_(j18,j17)")
        resultFolds.append("	*GM(b(9),j17,j16)")
        resultFolds.append("	*d_(j16,j15)")
        resultFolds.append("	*prop(b(7),b(8))")
        resultFolds.append("	*prop(b(9),b(10))")
        resultFolds.append("	*prop(b(19),b(20))")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd3l85 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed3l85 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed3l85 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few3l85 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few3l85 :")
        resultFolds.append("")
        resultFolds.append("")
        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))

    def test_3ltopprop_newfermions(self):
        diagram = Diagram()
        diagram.diagramNumber = 85
        diagram.preFactor = "(+1)*1"
        diagram.numLoops = 3
        diagram.externalMomenta = [["q1", 1, "ft", 1], ["q2", 2, "fT", 2]]
        diagram.internalMomenta = [["p1", 1, 2, "fT", "ft", 5, 6, 'M1'],\
             ["p2", 3, 1, "g", "g", 7, 8, ''], ["p3", 4, 2, "g", "g", 9, 10, ''], ["p4", 5, 3, "fT", "ft", 11, 12, 'M1'],\
             ["p5", 3, 5, "fT", "ft", 13, 14, 'M1'], ["p6", 6, 4, "fQ", "fq", 15, 16, ''], ["p7", 4, 6, "fQ", "fq", 17, 18, ''], ["p8", 6, 5, "g", "g", 19, 20, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        # Now test code generation
        config = Conf("test/newfermions.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFolds = []
        resultFolds.append("*--#[ d3l85 :")
        resultFolds.append("")
        resultFolds.append("	(+1)*1")
        resultFolds.append("	*fprop(p1,nh,nh,j6,j5)")
        resultFolds.append("	*Dg(nu7,nu8,p2)")
        resultFolds.append("	*Dg(nu9,nu10,p3)")
        resultFolds.append("	*fprop(p4,nh,nh,j12,j11)")
        resultFolds.append("	*fprop(p5,nh,nh,j14,j13)")
        resultFolds.append("	*fprop(p6,nl,nl,j16,j15)")
        resultFolds.append("	*fprop(p7,nl,nl,j18,j17)")
        resultFolds.append("	*Dg(nu19,nu20,p8)")
        resultFolds.append("	*fvertV(nh,nh,nu8,j5,i1)")
        resultFolds.append("	*fvertV(nh,nh,nu10,i2,j6)")
        resultFolds.append("	*fvertV(nh,nh,nu7,j13,j12)")
        resultFolds.append("	*fvertV(nl,nl,nu9,j17,j16)")
        resultFolds.append("	*fvertV(nh,nh,nu20,j11,j14)")
        resultFolds.append("	*fvertV(nl,nl,nu19,j15,j18)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d3l85 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd3l85 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	*d_(j6,j5)")
        resultFolds.append("	*prop(b(7),b(8))")
        resultFolds.append("	*prop(b(9),b(10))")
        resultFolds.append("	*d_(j12,j11)")
        resultFolds.append("	*d_(j14,j13)")
        resultFolds.append("	*d_(j16,j15)")
        resultFolds.append("	*d_(j18,j17)")
        resultFolds.append("	*prop(b(19),b(20))")
        resultFolds.append("	*GM(b(8),j5,i1)")
        resultFolds.append("	*GM(b(10),i2,j6)")
        resultFolds.append("	*GM(b(7),j13,j12)")
        resultFolds.append("	*GM(b(9),j17,j16)")
        resultFolds.append("	*GM(b(20),j11,j14)")
        resultFolds.append("	*GM(b(19),j15,j18)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd3l85 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed3l85 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed3l85 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few3l85 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few3l85 :")
        resultFolds.append("")
        resultFolds.append("")
        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))

    def test_muondecay_LO(self):
        diagram = Diagram()
        diagram.diagramNumber = 1
        diagram.preFactor = "(+1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [['q1', 1, 'fmu', 1], ['q2', 2, 'fMu', 2]]
        diagram.internalMomenta = [['p1', 2, 1, 'fEl', 'fel', 5, 6, 'M2'], ['p2', 1, 2, 'fNumu', 'fnumu', 7, 8, ''], ['p3', 1, 2, 'fNuel', 'fnuel', 9, 10, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        # Now test code generation
        config = Conf("test/fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFolds = []
        resultFolds.append("*--#[ d2l1 :")
        resultFolds.append("")
        resultFolds.append("	(+1)*1")
        resultFolds.append("	*fprop(p1,nel,nel,j6,j5)")
        resultFolds.append("	*fprop(p2,1,1,j8,j7)")
        resultFolds.append("	*fprop(p3,1,1,j10,j9)")
        resultFolds.append("	*fermicc(1,1,1,nel,j7,i1,j9,j6)")
        resultFolds.append("	*fermi(1,1,nel,1,i2,j8,j5,j10)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d2l1 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd2l1 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd2l1 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed2l1 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed2l1 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few2l1 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few2l1 :")
        resultFolds.append("")
        resultFolds.append("")

        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))

    def test_muondecay_NNLO(self):
        diagram = Diagram()
        diagram.diagramNumber = 6
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 4
        diagram.externalMomenta = [['q1', 1, 'fmu', 1], ['q2', 2, 'fMu', 2]]
        diagram.internalMomenta = [['p1', 3, 1, 'a', 'a', 5, 6, ''], ['p2', 1, 5, 'fMu', 'fmu', 7, 8, 'M1'], ['p3', 4, 2, 'a', 'a', 9, 10, ''],\
                                   ['p4', 6, 2, 'fMu', 'fmu', 11, 12, 'M1'], ['p5', 3, 4, 'fEl', 'fel', 13, 14, 'M2'], ['p6', 6, 3, 'fEl', 'fel', 15, 16, 'M2'],\
                                   ['p7', 4, 5, 'fEl', 'fel', 17, 18, 'M2'], ['p8', 5, 6, 'fNumu', 'fnumu', 19, 20, ''], ['p9', 5, 6, 'fNuel', 'fnuel', 21, 22, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        # Now test code generation
        config = Conf("test/fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFolds = []
        resultFolds.append("*--#[ d4l6 :")
        resultFolds.append("")
        resultFolds.append("	(-1)*1")
        resultFolds.append("	*Dg(nu5,nu6,p1)")
        resultFolds.append("	*fprop(p2,1,1,j8,j7)")
        resultFolds.append("	*Dg(nu9,nu10,p3)")
        resultFolds.append("	*fprop(p4,1,1,j12,j11)")
        resultFolds.append("	*fprop(p5,nel,nel,j14,j13)")
        resultFolds.append("	*fprop(p6,nel,nel,j16,j15)")
        resultFolds.append("	*fprop(p7,nel,nel,j18,j17)")
        resultFolds.append("	*fprop(p8,1,1,j20,j19)")
        resultFolds.append("	*fprop(p9,1,1,j22,j21)")
        resultFolds.append("	*fvertV(1,1,nu6,j7,i1)")
        resultFolds.append("	*fvertV(1,1,nu10,i2,j12)")
        resultFolds.append("	*fvertV(nel,nel,nu5,j13,j16)")
        resultFolds.append("	*fvertV(nel,nel,nu9,j17,j14)")
        resultFolds.append("	*fermicc(1,1,1,nel,j19,j8,j21,j18)")
        resultFolds.append("	*fermi(1,1,nel,1,j11,j20,j15,j22)")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("	#define TOPOLOGY \"arb\"")
        resultFolds.append("	#define INT1 \"arb\"")
        resultFolds.append("")
        resultFolds.append("*--#] d4l6 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqcd4l6 :")
        resultFolds.append("")
        resultFolds.append("	1")
        resultFolds.append("	;")
        resultFolds.append("")
        resultFolds.append("*--#] fqcd4l6 :")
        resultFolds.append("")
        resultFolds.append("*--#[ fqed4l6 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] fqed4l6 :")
        resultFolds.append("")
        resultFolds.append("*--#[ few4l6 :")
        resultFolds.append("	1")
        resultFolds.append("*--#] few4l6 :")
        resultFolds.append("")
        resultFolds.append("")

        self.assertEqual(diaGenerator.entryList[0],'\n'.join(resultFolds))


    def test_novel_multiple_lorentz_index_support(self):
        diagram = Diagram()
        diagram.diagramNumber = 666
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 0
        diagram.externalMomenta = [['q1', 1, 'fmu', 1], ['q2', 1, 'fMu', 1], ['q3', 2, 'fmu', 2], ['q4', 2, 'fMu', 2] ]
        diagram.internalMomenta = [['p1', 1, 2, 'X', 'X', 1, 2, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        # Now test code generation
        config = Conf("test/fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFold = """
*--#[ d0l666 :

        (-1)*1
        *d_(nu9n1,nu10n1)*d_(nu9n2,nu10n2)*d_(nu9n3,nu10n3)*d_(nu9n4,nu10n4)*d_(nu9n5,nu10n5)
        *XVert(nu9n1,nu9n2,nu9n3,nu9n4,nu9n5)
        *XVert(nu10n1,nu10n2,nu10n3,nu10n4,nu10n5)
        ;

        #define TOPOLOGY "arb"
        #define INT1 "arb"

*--#] d0l666 :

*--#[ fqcd0l666 :

        1
        ;

*--#] fqcd0l666 :

*--#[ fqed0l666 :
        1
*--#] fqed0l666 :

*--#[ few0l666 :
        1
*--#] few0l666 :
"""

        diaGenerator.entryList[0] = diaGenerator.entryList[0].replace("\t", "").replace("\n", "").replace(" ","")
        resultFold =resultFold.replace("\t", "").replace("\n", "").replace(" ","")

        self.assertEqual(diaGenerator.entryList[0],resultFold)



    def test_self_one_loop(self):
        diagram = Diagram()
        diagram.diagramNumber = 666
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 1
        diagram.externalMomenta = [['q1', 1, 'g', 1]]
        diagram.internalMomenta = [['p1', 1, 1, 'fq', 'fQ', 1, 1, 'M1']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        # Now test code generation
        config = Conf("test/topprop3l.conf",False, oldq2e=True)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        resultFold = """
*--#[ d1l666 :

        (-1)*1*nl
        *FT1(mu1)
        *FT1(-p1)
        ;

        #define TOPOLOGY "arb"
        #define INT1 "arb"

*--#] d1l666 :

*--#[ fqcd1l666 :

        1
        *GM(a(1),j4,j3)
        *d_(j4,j3)
        ;

*--#] fqcd1l666 :

*--#[ fqed1l666 :
        1
*--#] fqed1l666 :

*--#[ few1l666 :
        1
*--#] few1l666 :
"""

        diaGenerator.entryList[0] = diaGenerator.entryList[0].replace("\t", "").replace("\n", "").replace(" ","")
        resultFold =resultFold.replace("\t", "").replace("\n", "").replace(" ","")

        self.assertEqual(diaGenerator.entryList[0],resultFold)
        
    def test_one_loop_fermion_tadpoles(self):
        diagram = Diagram()
        diagram.diagramNumber = 1
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [['q1', 1, 'ft', 1],['q2', 2, 'fT', 2]]
        diagram.internalMomenta = [['p1', 1, 2, 'fT', 'ft', 6, 5, 'M1'],['p2', 1, 3, 'h', 'h', 8, 7, ''],['p3', 2, 4, 'h', 'h', 10, 9, ''],['p4', 3, 3, 'fT', 'ft', 12, 11, 'M1'],['p5', 4, 4, 'fT', 'ft', 14, 13, 'M1']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()
        self.assertEqual(len(diagram.fermionLines),3)


    def test_local_indices(self):
        diagram = Diagram()
        diagram.diagramNumber = 2
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [['q1', 1, 'fmu', 1], ['q2', 1, 'fMu', 2]]
        diagram.internalMomenta = [['p1', 1, 2, 'a', 'a', 2, 3, ''], ['p2', 2, 1, 'a', 'a', 4, 5, ''], ['p3', 2, 2, 'fmu', 'fMu', 6, 7, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        config = Conf("test/fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        self.assertIn("*f(c1,hallo1,wu1)*f(wu1,hallo1,c1)", diaGenerator.entryList[0])
        self.assertIn("*f(c2,hallo2,wu2)*f(wu2,hallo2,c2)", diaGenerator.entryList[0])


    def test_four_fermion_operator(self):
        diagram = Diagram()
        diagram.diagramNumber = 2
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 0
        diagram.externalMomenta = [['q1', 1, 'fb', 1], ['q2', 1, 'fB', 2], ['q3', 1, 'fs', 3], ['q4', 1, 'fS', 4]]
        diagram.internalMomenta = []
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        config = Conf("test/4fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        self.assertIn("fvertV(rho1,i1,i3)*fvertV(rho1,i2,i4)", diaGenerator.entryList[0])
        self.assertIn("GM(jj1,fe1,fe3)*GM(jj1,fe2,fe4)", diaGenerator.entryList[0])


    def test_matchRulesAndExtendDiagrams_multipleRules(self):
        config = Conf("test/4fermi.conf",False)
        feynmanRules = FeynmanRules(config)
        feynmanRules.vrtxLorentz = {("fB","fb","g"): ["","",""], ("fS","fs","g"): [""]}
        feynmanRules.propLorentz = {("g","g"): ["", ""], ("fS","fs"): [""]}

        diagram = Diagram()
        diagram.diagramNumber = 1
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 1
        diagram.externalMomenta = [['q1', 1, 'fb', 1], ['q2', 1, 'fB', 2], ['q3', 4, 'fb', 3], ['q4', 4, 'fB', 4]]
        diagram.internalMomenta = [['p1', 1, 2, 'g', 'g', 2, 3, ''],['p2', 2, 3, 'fs', 'fS', 4, 5, ''],['p3', 3, 2, 'fs', 'fS', 6, 7, ''],['p4', 3, 4, 'g', 'g', 8, 9, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        diaGenerator = DiaGenerator(config, feynmanRules)
        diagrams, matchedPropagators, matchedVertices = diaGenerator.matchRulesAndExtendDiagrams([diagram])

        self.assertEqual(len(diagrams),36)
        self.assertEqual(len(matchedPropagators),36)
        self.assertEqual(len(matchedVertices),36)

        self.assertIn({1: (("g","g"),0), 2: (("fS","fs"),0), 3: (("fS","fs"),0), 4: (("g","g"),0)},matchedPropagators)
        self.assertIn({1: (("g","g"),1), 2: (("fS","fs"),0), 3: (("fS","fs"),0), 4: (("g","g"),0)},matchedPropagators)

        self.assertIn({1: (('fB', 'fb', 'g'), 0), 2: (('fS', 'fs', 'g'), 0), 3: (('fS', 'fs', 'g'), 0), 4: (('fB', 'fb', 'g'), 0)},matchedVertices)
        self.assertIn({1: (('fB', 'fb', 'g'), 1), 2: (('fS', 'fs', 'g'), 0), 3: (('fS', 'fs', 'g'), 0), 4: (('fB', 'fb', 'g'), 1)},matchedVertices)
        self.assertIn({1: (('fB', 'fb', 'g'), 0), 2: (('fS', 'fs', 'g'), 0), 3: (('fS', 'fs', 'g'), 0), 4: (('fB', 'fb', 'g'), 2)},matchedVertices)


    def test_diaWithMultipleRules(self):
        config = Conf("test/4fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()

        diagram = Diagram()
        diagram.diagramNumber = 5
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 1
        diagram.externalMomenta = [['q1', 1, 'fb', 1], ['q2', 1, 'fB', 2], ['q3', 4, 'fb', 3], ['q4', 4, 'fB', 4]]
        diagram.internalMomenta = [['p1', 1, 2, 'g', 'g', 2, 3, ''],['p2', 2, 3, 'fs', 'fS', 4, 5, ''],['p3', 3, 2, 'fs', 'fS', 6, 7, ''],['p4', 3, 4, 'g', 'g', 8, 9, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        config = Conf("test/4fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        newDiagrams = diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        self.assertEqual(len(newDiagrams),16)
        self.assertEqual(len([s for s in diaGenerator.entryList if s.count("C1") == 2]), 4)
        self.assertEqual(len([s for s in diaGenerator.entryList if s.count("C2") == 2]), 4)
        self.assertEqual(len([s for s in diaGenerator.entryList if s.count("C1") == 1 and s.count("C2") == 1]), 8)
        self.assertEqual(len([s for s in diaGenerator.entryList if s.count("Dg1") == 2]), 4)
        self.assertEqual(len([s for s in diaGenerator.entryList if s.count("Dg2") == 2]), 4)
        self.assertEqual(len([s for s in diaGenerator.entryList if s.count("Dg1") == 1 and s.count("Dg2") == 1]), 8)

        self.assertListEqual([d.diagramNumber for d in newDiagrams], [i+1 for i in range(16)])
        self.assertEqual(diagram.diagramNumber, 5)


    def test_MassRule(self):
        config = Conf("test/4fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()

        diagram = Diagram()
        diagram.diagramNumber = 5
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 1
        diagram.externalMomenta = [['q1', 1, 'fmu', 1], ['q2', 1, 'fMu', 2], ['q3', 4, 'fmu', 3], ['q4', 4, 'fMu', 4]]
        diagram.internalMomenta = [['p1', 1, 2, 'g', 'g', 2, 3, ''],['p2', 2, 3, 'fmu', 'fMu', 4, 5, ''],['p3', 3, 2, 'fmu', 'fMu', 6, 7, ''],['p4', 3, 4, 'g', 'g', 8, 9, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        diagram.groupFermions()

        config = Conf("test/4fermi.conf",False)
        feynmanRulesReader = FeynmanRulesReader(config)
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules)
        diaGenerator.generateEntriesAndExtendDiagrams([diagram])

        self.assertIn("*MassDepVertex(M1,M1,0)", diaGenerator.entryList[0])
        self.assertIn("*Prop(-p2,M1)", diaGenerator.entryList[0])
        self.assertIn("*Prop(-p3,M1)", diaGenerator.entryList[0])
