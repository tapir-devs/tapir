from modules.outputHelpers import flatten
import unittest

class testHelperFunctions(unittest.TestCase):
    def test_flatten(self):
        self.assertListEqual(flatten([[1], 32, [2,[2]]]), [1, 32, 2, 2])
        self.assertListEqual(flatten([[1]]), [1])
        self.assertEqual(flatten("test"), "test")
        self.assertEqual(flatten(42), 42)