from modules.yamlReader import YamlReader
from modules.diagram import Diagram
from modules.config import Conf
import unittest

class testYamlReader(unittest.TestCase):
    def test_readDiagram(self):
        config = Conf("test/qqqq.conf",False)
        yamlReader = YamlReader(config)

        diagrams = yamlReader.readDiagrams("test/diagramsYamlTest.yaml")
        self.assertEqual(len(diagrams), 2)
        self.assertListEqual([d.diagramNumber for d in diagrams], [1, 2])


    def test_yamlToDiagram(self):
        yamlDict = {'diagramNumber': 1,
            'loops': 3,
            'preFactor': '(-1)*1',
            'externalLines': [
                {'incomingMomentum': 'q1', 'vertex': 1, 'incomingParticle': 'g'},
                {'incomingMomentum': 'q2', 'vertex': 2, 'incomingParticle': 'g'}
            ], 
            'internalLines': [
                {'momentum': 'p1', 'vertices': [3, 1], 'incomingParticle': 'fU', 'outgoingParticle': 'fu', 'mass': None},
                {'momentum': 'p2', 'vertices': [1, 4], 'incomingParticle': 'fU', 'outgoingParticle': 'fu', 'mass': None},
                {'momentum': 'p3', 'vertices': [5, 2], 'incomingParticle': 'fU', 'outgoingParticle': 'fu', 'mass': None},
                {'momentum': 'p4', 'vertices': [2, 6], 'incomingParticle': 'fU', 'outgoingParticle': 'fu', 'mass': 'M1'},
                {'momentum': 'p5', 'vertices': [5, 3], 'incomingParticle': 'g', 'outgoingParticle': 'g', 'mass': None},
                {'momentum': 'p6', 'vertices': [6, 3], 'incomingParticle': 'fU', 'outgoingParticle': 'fu', 'mass': None},
                {'momentum': 'p7', 'vertices': [4, 5], 'incomingParticle': 'fU', 'outgoingParticle': 'fu', 'mass': None},
                {'momentum': 'p8', 'vertices': [6, 4], 'incomingParticle': 'g', 'outgoingParticle': 'g', 'mass': None}
            ]}

        yamlReader = YamlReader(conf = Conf("test/qqqq.conf",False))

        diagram = yamlReader.yamlToDiagram(yamlDict)
        self.assertEqual(diagram.diagramNumber, 1)
        self.assertEqual(diagram.numLoops, 3)
        self.assertEqual(diagram.preFactor, '(-1)*1')
        self.assertEqual(diagram.numLegsIn, 2)
        self.assertEqual(diagram.numLegsOut, 0)
        self.assertListEqual(diagram.externalMomenta, [
            ['q1', 1, 'g', 'i1'],
            ['q2', 2, 'g', 'i2']
        ])
        self.assertListEqual(diagram.internalMomenta, [
            ['p1', 3, 1, 'fU', 'fu', 5, 6, ''],
            ['p2', 1, 4, 'fU', 'fu', 7, 8, ''],
            ['p3', 5, 2, 'fU', 'fu', 9, 10, ''],
            ['p4', 2, 6, 'fU', 'fu', 11, 12, 'M1'],
            ['p5', 5, 3, 'g', 'g', 13, 14, ''],
            ['p6', 6, 3, 'fU', 'fu', 15, 16, ''],
            ['p7', 4, 5, 'fU', 'fu', 17, 18, ''],
            ['p8', 6, 4, 'g', 'g', 19, 20, '']
        ])


    def test_readTopologies(self):
        config = Conf("test/qqqq.conf",False)
        yamlReader = YamlReader(config)

        diagrams = yamlReader.readTopologies("test/toposYamlTest.yaml")
        self.assertEqual(len(diagrams), 2)
        self.assertListEqual([d.diagramNumber for d in diagrams], [1, 2])


    def test_yamlToTopology(self):
        yamlDict = {
                "name": "topo42",
                "loops": 3,
                "externalLines": [ 
                    {"incomingMomentum": "q1", "vertex": 1},
                    {"outgoingMomentum": "q2", "vertex": 2}
                ],
                "internalLines": [
                    {"momentum": "p1", "vertices": [3, 1], "mass": "M1"},
                    {"momentum": "p2", "vertices": [1, 4], "mass": None},
                    {"momentum": "p3", "vertices": [5, 2], "mass": None},
                    {"momentum": "p4", "vertices": [2, 6], "mass": None},
                    {"momentum": "p5", "vertices": [5, 3], "mass": None},
                    {"momentum": "p6", "vertices": [6, 3]},
                    {"momentum": "p7", "vertices": [4, 5], "mass": None},
                    {"momentum": "p8", "vertices": [6, 4], "mass": None}
                ]
        }

        yamlReader = YamlReader(conf = Conf("test/qqqq.conf",False))

        diagram = yamlReader.yamlToTopology(yamlDict,1)
        self.assertEqual(diagram.diagramNumber, 1)
        self.assertEqual(diagram.name, "topo42")
        self.assertEqual(diagram.numLoops, 3)
        self.assertEqual(diagram.preFactor, 0)
        self.assertEqual(diagram.numLegsIn, 1)
        self.assertEqual(diagram.numLegsOut, 1)
        self.assertListEqual(diagram.externalMomenta, [
            ['q1', 1, 'dummyMasslessParticle', 1],
            ['q2', 2, 'dummyMasslessParticle', 2]
        ])
        self.assertListEqual(diagram.internalMomenta, [
            ['p1', 3, 1, 'dummyM1particle', 'dummyM1particle', 3, 1, 'M1'],
            ['p2', 1, 4, 'dummyMasslessParticle', 'dummyMasslessParticle', 1, 4, ''],
            ['p3', 5, 2, 'dummyMasslessParticle', 'dummyMasslessParticle', 5, 2, ''],
            ['p4', 2, 6, 'dummyMasslessParticle', 'dummyMasslessParticle', 2, 6, ''],
            ['p5', 5, 3, 'dummyMasslessParticle', 'dummyMasslessParticle', 5, 3, ''],
            ['p6', 6, 3, 'dummyMasslessParticle', 'dummyMasslessParticle', 6, 3, ''],
            ['p7', 4, 5, 'dummyMasslessParticle', 'dummyMasslessParticle', 4, 5, ''],
            ['p8', 6, 4, 'dummyMasslessParticle', 'dummyMasslessParticle', 6, 4, '']
        ])


    def test_readIntegralFamilies(self):
        config = Conf("test/qqqq.conf",False)
        yamlReader = YamlReader(config)

        families = yamlReader.readIntegralFamilies("test/familiesYamlTest.yaml")
        self.assertEqual(len(families), 2)
        self.assertListEqual([f.topoName for f in families], ["Int1", "Int2"])
        self.assertListEqual(families[0].analyticTopologyExpression, ["(k1 + q1)**2", "(k1 + q1 + q2)**2", "k1**2", "k2**2-M1**2", "(k1-k2)**2"])
        self.assertListEqual(families[0].loopMomenta, ["k1", "k2"])


    def test_yamlToIntegralFamilies(self):
        yamlDict = {
            "name": "topo42",
            "loop_momenta": ["k1", "k2"],
            "propagators": [ ["(k1 + q1)**2", 0], ["(k1 + q1 + q2)**2", 0], ["k1**2", 0], ["k2**2-M1**2", 0], ["(k1-k2)**2",0] ]
        }

        yamlReader = YamlReader(conf = Conf("test/qqqq.conf",False))

        family = yamlReader.yamlToIntegralFamily(yamlDict,1)

        self.assertEqual(family.topoName, "topo42")
        self.assertEqual(family.loopMomenta, ["k1", "k2"])
        self.assertEqual(family.analyticTopologyExpression, ["(k1 + q1)**2", "(k1 + q1 + q2)**2", "k1**2", "k2**2-M1**2", "(k1-k2)**2"])